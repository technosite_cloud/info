#!/bin/sh
java -version
javac -version
echo ${WERCKER_GIT_COMMIT}
echo ${WERCKER_GIT_BRANCH}
./gradlew --version
./gradlew dependencies
